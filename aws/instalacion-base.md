## Instalación base de las máquinas

### Instalación de Java 1.8

Se instala el paquete de JDK 1.8

```
sudo add-apt-repository ppa:openjdk-r/ppa
sudo apt-get update
sudo apt-get install openjdk-8-jdk
```

Se establece la variable de contexto JAVA_HOME

```
vi .bashrc
JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
source .bashrc
```

### Creación del directorio de instalación de aplicaciones

En el directorio /opt se instalarán las herramientas que forman parte de la solución

```
mkdir opt
```



* Para el caso de las instancias que requieren un volumen EBS adicional, como el caso de los datanodes, las instrucciones de montaje del volumen pueden encontrarse [aquí](https://gitlab.com/shflorez/kschool-pfm/blob/master/aws/montaje-EBS.md) .
