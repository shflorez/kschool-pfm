## Infraestructura

La infraestructura tecnológica se basa en Amazon Web Services.

### Tipos de instancias seleccionadas

|Tipo|vCPU|RAM|Volume|EBS|
|----|----|---|------|---|
|t2.micro|1 vCPU|1 GiB|8 GB(default)||
|t2.small|1 vCPU|2 GiB|8 GB||
|t2.small|1 vCPU|2 GiB|8 GB|30 GB|
|t2.large|2 vCPU|8 GiB|8 GB||
|t2.xlarge|4 vCPU|16 GiB|8 GB||

### Asignación Instancia-Servicio

|Servicio|Instancia|
|--------|---------|
|Zookeeper|t2.micro|
|Kafka|t2.small|
|Satori Client|t2.micro|
|Hadoop Master|t2.small|
|Hadoop Datanode|t2.small (con EBS)|
|Hadoop Datanode|t2.small (con EBS)|
|Druid|t2.xlarge|

### Elastic IP
Activada una Elastic IP para la instancia de Druid
