## Montaje EBS en las máquinas Datanodes

### Montaje de los EBS provisionados

Se monta el EBS en el punto `/mnt/ebs`:

```
sudo fdisk -l
sudo mkfs -t ext3 /dev/xvdb
sudo mkdir /mnt/ebs
sudo mount /dev/xvdb /mnt/ebs
```

Se comprueba que ya aparece montado:

```
df -h
```

### Montaje automático

Se establece el montaje automático de los EBS cuando se reinicie la máquina:

```
sudo cp /etc/fstab /etc/fstab.bak
sudo vi /etc/fstab
```

Se incorpora la siguiente línea en el fichero `/tc/fstab`:

```
/dev/xvdb               /mnt/ebs                ext3    defaults        0 0
```



