# Solución para la monitorización y predicción del uso de las estaciones Citi Bike en la ciudad de Nueva York


## Contexto funcional

Citi Bike (https://www.citibikenyc.com/) es el programa de bicicletas compartidas más grande del país, con 10,000 bicicletas y 600 estaciones en Manhattan, Brooklyn, Queens y Jersey City. Fue diseñado para viajes rápidos teniendo en cuenta la comodidad, y es una forma divertida y económica de moverse por la ciudad. Los usuarios de este programa, pueden coger una bicicleta en cualquiera de las más de 800 estaciones que existen en la ciudad de Nueva York, hacer el trayecto que deseen dentro de un tiempo limitado y volver a dejar la bicicleta en la estación más cercana a su destino.

El programa CitiBike comparte el estado de cada una de sus estaciones enviando los datos asociados en cientos de eventos al minuto mediante un canal de datos abiertos (Open Data). Adicionalmente, el site https://www.citibikenyc.com/system-data pueden encontrarse datasets relacionados con el uso y los usuarios del programa que pueden descargarse libremente.

A partir del los datos recibidos en streaming, el objetivo es monitorizar las estaciones para observar cuales de ellas son las más utilizadas, cuales las que menos, cuales se quedan sin bicicletas más frecuentemente, etc ; y por otro lado, poder predecir, en base al uso histórico, cuales serán las estaciones más utilizadas en los próximos 7 días.

## Infraestructura tecnológica

La infraestrucura se basará en servicios de AWS, principalmente instancias EC2 sobre los que se instalarán y ejecutarán los componentes arquitectónicos que darán forma a la solución. 

En [Maquinas Infra](https://gitlab.com/shflorez/kschool-pfm/blob/master/aws/maquinas-infra.md) se pueden consultar los tipos de instancias y las características de cada una de ellas.
Toda instancia tiene dos directorios importantes: el directorio `~/opt` donde se instalan las aplicaciones que conforman la solución y el directorio `~/pfm` que alberga todos los ficheros de configuración, scripts y jobs a ejecutar.
La instalación base de las máquinas está recogida [aquí] (https://gitlab.com/shflorez/kschool-pfm/blob/master/aws/instalacion-base.md).

## Arquitectura tecnológica 

### Componentes
<div align="center"><img src="_img/arq1.png" width="80%"></div>

#### Satori Client 
https://www.satori.com/
*Satori is a platform for any application with live data.  Live data refers to continually changing and instantly relevant content. Developers can use Satori to write complex production applications without having to design and deploy their own servers. 
At the core of the Satori platform is a realtime messaging service (RTM) of publish-subscribe architecture. Senders publish data in the form of messages to a channel. Recipients subscribe to the channels to receive the messages. RTM is constantly up: buffering, replicating, optionally evaluating and transforming, and forwarding the messages from publishers to subscribers.*

El cliente de Satori recibe los mensajes enviados por el stream de datos **NYC City Bike Stations** (*This channel contains updates from bike sharing stations provided by Citi Bike as part of NYC open data. The messages format is General Bikeshare Feed Specification (GBFS). specifications are defined at https://github.com/NABSA/gbfs/blob/master/gbfs.md*) y los va enviando conforme los recibe a un topic de kafka denominado *satori-nyc-bikes*

#### Zookeeper
Servicio de coordinación necesario para Kafka y Druid 

[Instalación Zookeeper](https://gitlab.com/shflorez/kschool-pfm/blob/master/zookeeper/zookeeper.md)

#### Kafka
Se ha configurado una cola kafka con un único broker y dos topics: 
1. *satori-nyc-bikes*: almacena los mensajes recibidos del cliente Satori 
2. *bikes*: almacena los mensajes adaptados por un job Spark para ser ingestados en Druid

[Instalación Kafka](https://gitlab.com/shflorez/kschool-pfm/blob/master/kafka/kafka.md)

#### Flume
Persiste en HDFS los mensajes almacenados en el topic *satori-nyc-bikes*

[Instalación Flume](https://gitlab.com/shflorez/kschool-pfm/blob/master/hadoop/flume.md)

#### Clúster Hadoop
El cluster está formado por un nodo máster como namenode y dos esclavos como datanodes. 3 instancias EC2 de AWS.

El cluster tiene 3 objetivos:
1. el almacenamiento persistente de los mensajes originales y procesados
2. el almacenamiento de los segmentos de Druid (utilización de HDFS como DeepStorage de Druid)
3. la ejecución sobre YARN de procesos Spark y SparkStreaming para la adaptación de los mensajes y preparación de datos que sirven como entrada a Prophet (predicción)

[Instalación Hadoop](https://gitlab.com/shflorez/kschool-pfm/blob/master/hadoop/hadoop.md)

#### Spark
Se utiliza para la ejecución de los procesos sobre el cluster YARN.

[Instalación Spark](https://gitlab.com/shflorez/kschool-pfm/blob/master/hadoop/spark.md)

#### Prophet
Se utiliza para la realización de las predicciones sobre los datos históricos de los estados de las estaciones.

[Instalación Prophet](https://gitlab.com/shflorez/kschool-pfm/blob/master/druid/prophet.md)

#### Druid
Almacena los datos de las estaciones en tiempo real, junto con las agregaciones necesarias para la monitorización del estado de las estaciones.

[Instalación Druid](https://gitlab.com/shflorez/kschool-pfm/blob/master/druid/druid.md)

#### MySQL
Esta base de datos relacional se utiliza para albergar los metadatos de Druid (en lugar de la Derby que incluye por defecto) y para almacenar las predicciones de las estaciones. 

#### Metabase
Mediante cuadros de mando en Metabase se pueden monitorizar las métricas de Druid y consultar las predicciones de Prophet.
Adicionalmente, se instala el notebook de Jupyter para representar visualmente la predicción mediante matplotlib.

[Instalación Metabase](https://gitlab.com/shflorez/kschool-pfm/blob/master/druid/visual.md)

[Instalación Jupyter](https://gitlab.com/shflorez/kschool-pfm/blob/master/druid/jupyter.md)

<div align="center"><img src="_img/dashboard.png" width="50%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="_img/prophet.png" width="28%"></div>

### Procesos
A continuación se enumeran los procesos que se ejecutan y ficheros específicos necesarios para llevar a cabo la solución.

#### Instancia Satori
[`Satori2Kafka.py`](https://gitlab.com/shflorez/kschool-pfm/blob/master/satori/pfm/Satori2Kafka.py): programa python que lee los mensajes del stream de datos y los va enviando a un topic de Kafka.

#### Instancia Kakfka
(Ninguno)

#### Master del clúster de Hadoop
[`StartFlumeAgent.sh`](https://gitlab.com/shflorez/kschool-pfm/blob/master/hadoop/pfm/StartFlumeAgent.sh): Script que arranca el agente Flume para perisitir los mensajes del topic de Kafka en HDFS

[`flume-kafka-source-hdfs-sink.conf`](https://gitlab.com/shflorez/kschool-pfm/blob/master/hadoop/pfm/flume-kafka-source-hdfs-sink.conf): En esta instancia está instalado Flume y requiere un nuevo fichero de configuración para establecer los parámetros de source (kafka) y sink (HDFS).


[`KafkaToDruidAdapter.sh`](https://gitlab.com/shflorez/kschool-pfm/blob/master/hadoop/pfm/KafkaToDruidAdapter.sh): Script que lanza un proceso de Spark Streaming que incorpora un nuevo campo al JSON que forma el mensaje de entrada y que se requiere para que pueda ser aceptado por Druid. El campo que determina el timestamp en el mensaje se cambia a formato ISO y se incorpora como un nuevo campo.

[`KafkaToDruidAdapter.py`](https://gitlab.com/shflorez/kschool-pfm/blob/master/hadoop/pfm/KafkaToDruidAdapter.py): Script Python lanzado por el script shell anterior.
<div align="center"><img src="_img/jsons.png" width="30%"></div>


[`FlumeToMinABperDay.sh`](https://gitlab.com/shflorez/kschool-pfm/blob/master/hadoop/pfm/FlumeToMinABperDay.sh): Script que calcula con Spark, para el día anterior y para cada estación, el número mínimo de bicicletas disponibles. Se ejecuta todos los días a las 00:30 horas.

[`FlumeToMinABperDay.py`](https://gitlab.com/shflorez/kschool-pfm/blob/master/hadoop/pfm/FlumeToMinABperDay.py): Script Python lanzado por el script shell anterior.
<div align="center"><img src="_img/minab.png" width="50%"></div>


[`MinABperDayToHistorical.sh`](https://gitlab.com/shflorez/kschool-pfm/blob/master/hadoop/pfm/MinABperDayToHistorical.sh): Script que genera con Spark el histórico por estación de las bicicletas disponibles y que será la base de la predicción. Se ejecuta todos los días a las 01:00 horas.

[`MinABperDayToHistorical.py`](https://gitlab.com/shflorez/kschool-pfm/blob/master/hadoop/pfm/MinABperDayToHistorical.py): Script Python lanzado por el script shell anterior.
<div align="center"><img src="_img/hist.png" width="50%"></div>


#### Druid

[`kafka-bikes.json`](https://gitlab.com/shflorez/kschool-pfm/blob/master/druid/pfm/kafka-bikes.json): Especificación para el supervisor de Druid sobre Servicio de Indexación de Kafka que incluye la definición del datasource 

[`MakePredictionsPROPHET.sh`](https://gitlab.com/shflorez/kschool-pfm/blob/master/druid/pfm/MakePredictionsPROPHET.sh): Ejecuta el proceso de predicción según Prophet para una estación en concreto y almacena el resultado en la base MySQL. Se ejecuta todos los lunes a las 00:05 horas

[`MakePredictionsPROPHET.py`](https://gitlab.com/shflorez/kschool-pfm/blob/master/druid/pfm/MakePredictionsPROPHET.py): Script Python lanzado por el script shell anterior.

## Ejecución

1. Login en la máquina Zookeeper. Ejecución de los comandos:
```
~/opt/zookeeper-3.4.10/bin/zkServer.sh start
```
2. Login en la máquina Kafka. Ejecución del script:
```
./pfm/demo.sh
```
3. Login en la máquina de Druid. Ejecución del script:
```
./pfm/demo.sh
```
4. Login en la máquina máster del cluster Hadoop. Ejecución de los scripts:
```
start-all.sh
./pfm/demo.sh
```
5. Login en la máquina Satori. Ejecución de los comandos:
```
nohup python ~/pfm/Satori2Kafka.py >> satori.out
```
6. En la máquina de Druid, arrancar el notebook Jupyter según las indicaciones de 
[Ejecutar Jupyter en AWS](https://gitlab.com/shflorez/kschool-pfm/blob/master/druid/jupyter.md#ejecución)

















