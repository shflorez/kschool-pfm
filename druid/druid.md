## Instalación y configuración de Druid


### Instalación

#### Instalación de MySQL
```
sudo apt-get update
sudo apt-get install mysql-server

#[instalar mysql user root con pass root]

# comprobar que está ejecutándose el servicio
systemctl status mysql.service
```


#### Instalación de Druid
```
wget http://static.druid.io/artifacts/releases/druid-0.10.1-bin.tar.gz

tar -xvf druid-0.10.1-bin.tar.gz
```

#### Instalación de la extensión de MySQL para Druid
```
cd ~/opt/druid-0.10.0/extensions

wget http://static.druid.io/artifacts/releases/mysql-metadata-storage-0.10.1.tar.gz

tar -xvf mysql-metadata-storage-0.10.1.tar.gz
```


### Configuración

#### Configuración de MySQL para albergar el metadata Storage de Druid

Nos logamos en MySQL como root

```
mysql -uroot -proot
```

Creamos el usuario y la base de datos del metastore

```
create user 'druid'@'localhost' identified by 'diurd' password expire never;
-- create a druid database, make sure to use utf8 as encoding
CREATE DATABASE druid DEFAULT CHARACTER SET utf8;
grant all on druid.* to 'druid'@'localhost';
flush privileges;
use druid;
show tables;
```

Comprobamos que el usuario existe y tiene acceso a Druid

```
select host, user from mysql.user;
```

Podemos logarnos con el usuario recién creado:

```
mysql -h localhost -udruid -p
```


#### Configuración de Druid

Editar el fichero `druid-dist/conf/druid/_common/common.runtime.properties`

- eliminamos `druid-s3-extensions` de la propiedad `druid.extensions.loadList`
- añadimos `druid-kafka-indexing-service` a la propiedad `druid.extensions.loadList` 
- se modifica la propiedad `druid.zk.service.host=localhost:2181`

Comentamos el metadata storage para Derby y se incorpora:

```
druid.metadata.storage.type=mysql
druid.metadata.storage.connector.connectURI=jdbc:mysql://localhost:3306/druid
druid.metadata.storage.connector.user=druid
druid.metadata.storage.connector.password=diurd
```

- añadimos `druid-hdfs-storage` a la propiedad `druid.extensions.loadList`

- se comenta `local disk` para el `druid.storage` y se incorporan:

```
# For HDFS (make sure to include the HDFS extension and that your Hadoop config files in the cp):
druid.storage.type=hdfs
druid.storage.storageDirectory=hdfs://localhost:9000/druid/segments
```


Se realizan las modificaciones de memoria en los ficheros de configuración de cada uno de los servicios.


### Ejecución
```
bin/init (únicamente la primera vez)

bin/coordinator.sh start

bin/broker.sh start

bin/historical.sh start

bin/overlord.sh start

bin/middleManager.sh start
```

Se comprueba que están todos los servicios levantados

```
ps aux | grep druid | grep java
```


#### Lanzar la ingesta en Druid (kafka indexing service)
Para ejecutar la tarea de indexación debemos subir el fichero mediante una peticion POST al Overlord.

```
curl -X POST -H 'Content-Type: application/json' -d @kafka-bikes.json http://localhost:8090/druid/indexer/v1/supervisor
```

Ahora si la tarea se ha levantado podemos verificar que esta funcionando si miramos en la interfaz web del overlod

```
http://${OVERLORD_IP}:8090/console.html
```

#### Parar la tarea de indexación
Podemos parar la indexación para generar segmentos con la siguiente petici´ón
```
curl -X POST -H 'Content-Type: application/json' http://localhost:8090/druid/indexer/v1/supervisor/bikes-nyc/shutdown
```

