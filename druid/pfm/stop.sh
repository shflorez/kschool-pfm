#!/bin/bash

# Arranque de Druid
cd ~/opt/druid-0.10.1

bin/middleManager.sh stop

sleep 5

bin/overlord.sh stop

sleep 5

bin/historical.sh stop

sleep 5

bin/broker.sh stop

sleep 5

bin/coordinator.sh stop 

