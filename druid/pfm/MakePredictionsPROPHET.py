# -*- coding: utf-8 -*-

"""Prediction.

Generate the prediction for a station with Prophet and stores it as a table
in MySQL database.
"""

import sys
import pandas as pd
from datetime import date
from fbprophet import Prophet
from hdfs import InsecureClient
from sqlalchemy import create_engine

path = "/pfm/historical/"
hdfsClient = InsecureClient('http://172.31.95.160:50070', user='ubuntu')


def getpath(station):
    """Return the HDFS path to the station historical file."""
    p = path + station
    list = hdfsClient.list(p, status=False)
    for e in list:
        if e.endswith("csv"):
            finalp = p + "/" + e
            break
    return finalp


def storeprediction(fc, st):
    """Store the prediction on MySQL."""
    tablename = st + "_" + date.today().strftime("%d%m%Y")

    engine = create_engine(
        'mysql://prophet:profeta@127.0.0.1:3306/prophet?charset=utf8')
    connection = engine.connect()

    fc.to_sql(name=tablename, con=engine, if_exists='replace',
              index=False, chunksize=20000)
    connection.close()


def main():
    """Run the main function."""
    station = str(sys.argv[1])
    p = getpath(station)

    with hdfsClient.read(p) as reader:
        df = pd.read_csv(reader)

    m = Prophet()
    m.fit(df)

    future = m.make_future_dataframe(periods=7)
    future.tail(7)

    forecast = m.predict(future)
    forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail(7)

    storeprediction(forecast, station)


if __name__ == '__main__':
    main()
