#!/bin/bash

# Arranque de Druid
cd ~/opt/druid-0.10.1

bin/coordinator.sh start

sleep 5

bin/broker.sh start

sleep 5

bin/historical.sh start

sleep 5

bin/overlord.sh start

sleep 5

bin/middleManager.sh start

sleep 5


# Arranque de Metabse
cd ~/opt/metabase-0.26
nohup java -jar metabase.jar >> metabase.out &

# Arranque de Grafana
#cd ~/opt/grafana-4.5.2
#nohup bin/grafana-server start >> grafana.out &

