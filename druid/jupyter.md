## Instalación y configuración de Jupyter Notebook


### Instalación

```
sudo pip install --upgrade pip
sudo pip install jupyter
```

### Ejecución
```
jupyter notebook --no-browser --port=8888
```
Copiar el enlace que se muestra por consola, similar al siguiente:

```
http://localhost:8888/?token=227b2a0c3d30b82ae18261409718957333d402125bf4535b
```

### Conexión al servidor Jupyter
Abrir una conexión ssh al servidor mediante la ejecución del siguiente comando:

```
ssh -i "kschool.pem" -L 8000:localhost:8888 ubuntu@54.227.133.226
```

*-i* Specifies an alternate identification file to use for public key authentication.

*-L* specifies that the given port on the local (client) host is to be forwarded to the given host and port on the remote side (AWS). This means that whatever is running on the second port number (i.e. 8888) on AWS will appear on the first port number (i.e. 8000) on your local computer. 

Posteriormente, abrir un navegador web e ir al enlace copiado anteriormente
