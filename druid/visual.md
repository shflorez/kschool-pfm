## Herramientas de visualización

### Metabase
```
cd ~/opt
mkdir metabase-0.26
cd ~/opt/metabase-0.26
wget http://downloads.metabase.com/v0.26.2/metabase.jar

java -jar ~/opt/metabase-0.26/metabase.jar
```

-------

### Grafana

#### Instalación

```
cd ~/opt
wget https://s3-us-west-2.amazonaws.com/grafana-releases/release/grafana-4.5.2.linux-x64.tar.gz
tar -zxvf grafana-4.5.2.linux-x64.tar.gz

cd ~/opt/grafana-4.5.2
mkdir plugins
```

#### Instalación del conector con Druid

Existen 2 formas:

1. Desde Github:
```
~/opt/grafana-4.5.2/plugins
git clone https://github.com/grafana-druid-plugin/druidplugin.git
```

2. Desde la página oficial de Grafana:
```
bin/grafana-cli --pluginsDir ~/opt/grafana-4.5.2/plugins plugins install abhisant-druid-datasource
```

#### Configuración
Indicar el directorio donde se encuentran los plugins:
```
vi ~/opt/grafana-4.5.2/conf/defaults.ini
#plugins = data/plugins
plugins = plugins
```

Reiniciamos el servicio: `bin/grafana-server restart`

Se accede a `http://IP:3000/login`

-------

### Superset

```
# Instalar OS dependencies
sudo apt-get install build-essential libssl-dev libffi-dev python-dev python-pip libsasl2-dev libldap2-dev

# Install superset
pip install superset

# Create an admin user (you will be prompted to set username, first and last name before setting a password)
fabmanager create-admin --app superset

# Initialize the database
superset db upgrade

# Load some data to play with
superset load_examples

# Create default roles and permissions
superset init

# Start the web server on port 8088, use -p to bind to another port
superset runserver

# To start a development web server, use the -d switch
# superset runserver -d

# Importante: recordar que el coordinator está escuchando en el puerto 8071

```