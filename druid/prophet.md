## Instalación y configuración de Prophet


### Instalación

```
sudo apt-get install python-pip
export LC_ALL=C

sudo pip install pystan
sudo pip install fbprophet
sudo pip install hdfs

sudo pip install SQLAlchemy
sudo apt-get install python-tk
sudo apt-get install python-dev libmysqlclient-dev
sudo pip install MySQL-python
```


#### Configuración de MySQL para albergar el resultado de las predicciones con Prophet

Nos logamos en MySQL como root

```
mysql -uroot -proot
```

Creamos el usuario y la base de datos

```
create user 'prophet'@'localhost' identified by 'profeta' password expire never;
CREATE DATABASE prophet DEFAULT CHARACTER SET utf8;
grant all on prophet.* to 'prophet'@'localhost';
flush privileges;
use prophet;
show tables;
```

Comprobamos que el usuario existe y tiene acceso a Druid

```
select host, user from mysql.user;
```

Podemos logarnos con el usuario recién creado:

```
mysql -h localhost -uprophet -p
```


### Ejecución
La predicción se realiza sobre una estación concreta, por lo que es necesario pasarle como argumento al programa el identificador de la estación. El objetivo es que esta predicción se realice semana a semana para una o un conjunto de estaciones. A modo de ejemplo, se programa la ejecución de la predicción para la estación número 2006.

Editar el fichero crontab `crontab -e` e incorporar la ejecución del fichero los lunes a las 00:05

```
05 0 * * 1 python /home/ubuntu/pfm/MakePredictionsPROPHET.py 2006
```

