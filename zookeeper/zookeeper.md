## Instalación y configuración de Zookeeper

### Instalación

```
wget http://apache.rediris.es/zookeeper/stable/zookeeper-3.4.10.tar.gz
tar -xvf zookeeper-3.4.10.tar.gz 
cp zoo_sample.cfg zoo.cfg
```

### Ejecución

```
cd opt/zookeeper-3.4.10/
bin/zkServer.sh start
```

Puede comprobarse que se ha arrancado correctamente:

```
bin/zkCli.sh -server localhost:2181
ls /
```