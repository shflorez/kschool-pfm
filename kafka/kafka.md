## Instalación y configuración de Kafka

### Instalación de componentes

Los siguientes componentes son necesarios para la ejecución de los scripts de Python

Instalación de pip (instalador de paquetes Python)

```
sudo apt-get install python-pip
export LC_ALL=C
```
Se instala el SDK de Satori (https://www.satori.com/)

```
sudo pip install satori-rtm-sdk
```

Se instala el paquete de Kafka para Python

```
sudo pip install kafka-python
```


#### Instalación de Kafka

```
cd ~/opt
wget http://apache.rediris.es/kafka/0.11.0.1/kafka_2.11-0.11.0.1.tgz
tar -xvf kafka_2.11-0.11.0.1.tgz
cd kafka_2.11-0.11.0.1/
```

Editar el fichero `/conf/server.properties` para modificar la propiedad
```
zookeeper.connect=172.31.84.69:2181
```


#### Ejecución de Kafka

```
bin/kafka-server-start.sh config/server.properties
```

Para ejecutar en segundo plano:

```
bin/kafka-server-start.sh -daemon config/server.properties
```

Para comprobar que Kakfa se ha levantado correctamente, desde la máquina donde se está ejecutando Zookeeper se ejecuta:

```
bin/zkCli.sh -server localhost:2181
ls /brokers/ids
->[0]
```




#### Ejecución del script Satori2Kafka.py

El script **Satori2Kafka.py** captura los eventos de stream de open data de Satori relativos a los estados de las estaciones de bicicletas de Nueva York y los envía a un topic de Kafka denominado **satori-nyc-bikes**.

```
cd ~/pfm

nohup python Satori2Kafka.py >> satori.out
```


---

**NOTA: Algunos comandos útiles**

Comprobación de los topics de Kafka
```
bin/kafka-topics.sh --list --zookeeper 172.31.84.69:2181
```

Imprimir por consola el contenido de un topic
```
bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic satori-nyc-bikes --new-consumer --consumer.config config/consumer.properties
```
