## Instalación y configuración del cluster Hadoop


### Instalación

Crear un grupo en terminator denominado Hadoop, abrir una terminal por nodo (master, dn1, dn2) y broadcast a grupo Hadoop

```
cd ~/opt
wget http://apache.rediris.es/hadoop/common/hadoop-2.6.5/hadoop-2.6.5.tar.gz
tar -zxf hadoop-2.6.5.tar.gz
mv hadoop-2.6.5 ~/opt/hadoop
export HADOOP_PREFIX=~/opt/hadoop
```

Modificar el fichero .bashrc del usuario hadoop, para añadir al path los directorios de binarios de Hadoop
```
export HADOOP_HOME=/home/ubuntu/opt/hadoop
export YARN_CONF_DIR=$HADOOP_HOME/etc/hadoop
export PATH=$PATH:$HADOOP_HOME/bin:$HADOOP_HOME/sbin
```

Para cada nodo, modificar el fichero `~/opt/hadoop/etc/hadoop/hadoop-env.sh` sustituir 
```
export JAVA_HOME=${JAVA_HOME}
```
por 
```
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
```

### Configuración

#### Communication password less
para cada nodo
```
ssh-keygen -t rsa
#Press enter for each line 
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
```

#### core_site.xml

para cada nodo, modificar el fichero `~/opt/hadoop/etc/hadoop/core-site.xml`
```xml
<configuration>
  <property>
    <name>fs.default.name</name>
    <value>hdfs://172.31.95.160:9000</value>
  </property>
  <property>
    <name>hadoop.tmp.dir</name>
    <value>/home/ubuntu/opt/hadoop/tmp</value>
  </property>
</configuration>
```


#### SET yarn-site.xml
para cada nodo, modificar el fichero `~/opt/hadoop/etc/hadoop/yarn-site.xml`
```xml
<configuration>
  <!-- Site specific YARN configuration properties -->
  <property>
    <name>yarn.nodemanager.aux-services</name>
    <value>mapreduce_shuffle</value>
  </property>
  <property>
    <name>yarn.nodemanager.aux-services.mapreduce.shuffle.class</name>
    <value>org.apache.hadoop.mapred.ShuffleHandler</value>
  </property>
  <property>
    <name>yarn.resourcemanager.hostname</name>
    <value>172.31.95.160</value>
  </property>
  <property>
    <name>yarn.log-aggregation-enable</name>
    <value>true</value>
  </property>
  <property>
    <name>yarn.nodemanager.vmem-check-enabled</name>
    <value>false</value>
  </property>
  <property>
    <name>yarn.nodemanager.disk-health-checker.max-disk-utilization-per-disk-percentage</name>
    <value>95</value>
  </property>
</configuration>
```

#### SET Data dirs (SOLO DATANODES)
```
sudo mkdir -p /mnt/ebs/hdfs/data
sudo chown -R ubuntu:ubuntu /mnt/ebs/hdfs/data
```

#### hdfs-site.xml
Modificar el fichero `~/opt/hadoop/etc/hadoop/hdfs-site.xml`
```xml
<configuration>
  <property>
    <name>dfs.replication</name>
    <value>2</value>
  </property>
  <property>
    <name>dfs.datanode.data.dir</name>
    <value>file:///mnt/ebs/hdfs/data</value>
  </property>
</configuration>
```



#### Communication password less (SOLO NAMENODE)
Ejecutar en en el namenode 
```
ssh-keygen
cat /home/ubuntu/.ssh/id_rsa.pub
```
y añadir el contenido a 
```
~/.ssh/authorized_keys
```
en cada uno de los datanodes

#### hdfs-site.xml
En el master, modificar el fichero `~/opt/hadoop/etc/hadoop/hdfs-site.xml`
```xml
<configuration>
  <property>
    <name>dfs.replication</name>
    <value>2</value>
  </property>
</configuration>
```

#### mapred-site.xml
Modificar el fichero `~/opt/hadoop/etc/hadoop/mapred-site.xml` (copiar de mapred-site.xml.template)
```xml
<configuration>
  <property>
    <name>mapreduce.jobtracker.address</name>
    <value>172.31.95.160:54311</value>
  </property>
  <property>
    <name>mapreduce.framework.name</name>
    <value>yarn</value>
  </property>
</configuration>
```


#### Ficheros masters y slaves
Crear el fichero **masters** dentro de `~/opt/hadoop/etc/hadoop` e incorporar la IP privada del namenode

Crear el fichero **slaves** dentro de `~/opt/hadoop/etc/hadoop` e incorporar las IPs de los datanodes y eliminar el 'localhost'


#### Ejecución 

Se da formato al sistema de ficheros de Hadoop
```
hadoop namenode -format
```

Y se arrancan los servicios
```
ejectuar un start-all.sh
```


También pueden ejecutarse los servicios de forma independiente:
1. Se arrancan los servicios del HDFS (maquina:50070): `start-dfs.sh`
2. Se arrancan los servicios de YARN (maquina:8088): `start-yarn.sh`
3. Se arranca el job-history-server (maquina:19888): `mr-jobhistory-daemon.sh start historyserver`


Comprobamos que podemos ejecutar un proceso map-reduce sobre el cluster yarn
```
hadoop jar $HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-examples-2.6.5.jar pi 10
```