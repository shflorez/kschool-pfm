## Instalación y configuración de Spark


### Instalación

#### Instalación de Python y paquetes
```
sudo apt-get install python-pip
export LC_ALL=C
sudo pip install kafka-python
sudo pip install hdfs
```

#### Instalación de Spark (sólo en Máster)
```
cd ~/opt
wget https://d3kbcqa49mib13.cloudfront.net/spark-2.1.1-bin-hadoop2.6.tgz
tar -xvf spark-2.1.1-bin-hadoop2.6.tgz
```

Comprobamos que podemos ejecutar un proceso spark sobre el cluster yarn

```
./bin/spark-submit --master yarn --executor-memory 2G examples/src/main/python/pi.py 100
```


#### Instalación del paquete de integración Spark Streaming + Kakfa
Se crea el directorio `external` para albergar la dependencia de Kafka
```
mkdir ~/opt/spark/spark-2.1.1-bin-hadoop2.6/external
```
Se copia el fichero `spark-streaming-kafka-0-8-assembly_2.11-2.1.1.jar` en el directorio anterior


### Ejecución

#### Job de Spark Streaming - KafkaToDruidAdapter.py 
```
# lanzar la ejecución
spark-submit --master yarn --jars /home/ubuntu/opt/spark-2.1.1-bin-hadoop2.6/external/spark-streaming-kafka-0-8-assembly_2.11-2.1.1.jar --executor-memory 2G /home/ubuntu/pfm/KafkaToDruidAdapter.py
```

#### Job Spark - FlumeToMinABperDay.py

Copiar el fichero `FlumeToMinABperDay.py` en `~/pfm`

Crear un fichero `FlumeToMinABperDay.sh` con el siguiente contenido

```
#!/bin/bash

export HADOOP_HOME=/home/ubuntu/opt/hadoop
export YARN_CONF_DIR=$HADOOP_HOME/etc/hadoop
export SPARK_HOME=/home/ubuntu/opt/spark-2.1.1-bin-hadoop2.6
export PATH=$PATH:$HADOOP_HOME/bin:$HADOOP_HOME/sbin:$SPARK_HOME/bin

spark-submit --master yarn --executor-memory 1G --driver-memory 1G /home/ubuntu/pfm/FlumeToMinABperDay.py
```

Editar el fichero crontab `crontab -e` e incorporar la ejecución del fichero sh a las 00:30AM todos los días

```
30 0 * * * /home/ubuntu/pfm/FlumeToMinABperDay.sh >> /home/ubuntu/pfm/flume.log 2>&1
```


#### Job Spark - MinABperDayToHistorical.py

Copiar el fichero `MinABperDayToHistorical.py` en `~/pfm`

Crear un fichero `MinABperDayToHistorical.sh` con el siguiente contenido

```
#!/bin/bash

export HADOOP_HOME=/home/ubuntu/opt/hadoop
export YARN_CONF_DIR=$HADOOP_HOME/etc/hadoop
export SPARK_HOME=/home/ubuntu/opt/spark-2.1.1-bin-hadoop2.6
export PATH=$PATH:$HADOOP_HOME/bin:$HADOOP_HOME/sbin:$SPARK_HOME/bin

spark-submit --master yarn --executor-memory 1G --driver-memory 1G /home/ubuntu/pfm/MinABperDayToHistorical.py
```
