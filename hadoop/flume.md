## Instalación y configuración de Flume

#### Instalación
```
cd ~/opt
wget http://apache.rediris.es/flume/1.8.0/apache-flume-1.8.0-bin.tar.gz; tar -xvf apache-flume-1.8.0-bin.tar.gz
```


#### Configuración

Incorporar las variables de entorno

```
export FLUME_HOME=/home/ubuntu/opt/apache-flume-1.8.0-bin
export PATH=$PATH:$FLUME_HOME/bin
```


Crear fichero `flume-kafka-source-hdfs-sink.conf` en el directorio `/conf` e incorporar el siguiente contenido:

```
agent1.sources = kafka-source
agent1.channels = memory-channel
agent1.sinks = hdfs-sink

agent1.sinks.hdfs-sink.channel = memory-channel
agent1.sources.kafka-source.channels = memory-channel

agent1.sources.kafka-source.type = org.apache.flume.source.kafka.KafkaSource
agent1.sources.kafka-source.kafka.bootstrap.servers = 172.31.90.185:9092
agent1.sources.kafka-source.kafka.topics = satori-nyc-bikes
agent1.sources.kafka-source.kafka.consumers.group.id = flume

agent1.channels.memory-channel.type = memory
agent1.channels.memory-channel.capacity = 10000
agent1.channels.memory-channel.transactionCapacity = 1000

agent1.sinks.hdfs-sink.type = hdfs
agent1.sinks.hdfs-sink.hdfs.path = hdfs://172.31.95.160:9000/pfm/raw/%Y-%m-%d

agent1.sinks.hdfs-sink.hdfs.rollInterval = 3600
agent1.sinks.hdfs-sink.hdfs.rollSize = 0
agent1.sinks.hdfs-sink.hdfs.rollCount = 0
agent1.sinks.hdfs-sink.hdfs.fileType = DataStream
agent1.sinks.hdfs-sink.hdfs.timeZone = America/New_York
```


#### Ejecución
Arrancar el agente mediante el comando 

```
bin/flume-ng agent --conf conf -conf-file conf/flume-kafka-source-hdfs-sink.conf --name agent -Dflume.root.logger=INFO,console
```