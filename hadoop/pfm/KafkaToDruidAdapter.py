# -*- coding: utf-8 -*-

"""Satori client.

Stores the messages from Satori open data channel into a Kafka topic.
"""

import json
import datetime
from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils
from kafka import KafkaProducer


brokers = "172.31.90.185:9092"
topicIN = "satori-nyc-bikes"
topicOUT = "bikes"


def changetimestamp(dicti):
    """Generate last comunication time in ISO format."""
    lct = dicti['lastCommunicationTime']
    lctdate = datetime.datetime.strptime(lct, '%Y-%m-%d %I:%M:%S %p')
    lctiso = lctdate.strftime("%Y-%m-%dT%H:%M:%SZ")
    dicti['lastCommunicationTimeISO'] = lctiso
    return dicti


def send(iterator):
    """Send the final message to Kafka topic."""
    producer = KafkaProducer(bootstrap_servers=brokers)
    for record in iterator:
        msg = json.dumps(record, ensure_ascii=False)
        producer.send(topicOUT, msg.encode())
        producer.flush()


def sender(message):
    """Iterate over every partition."""
    message.foreachPartition(send)


def main():
    """Run main function."""
    sc = SparkContext(appName="KafkaToDruidAdpater")
    ssc = StreamingContext(sc, 10)

    kvs = KafkaUtils.createDirectStream(
        ssc, [topicIN], {"metadata.broker.list": brokers})

    # Obtengo los strings de satori
    jsons = kvs.map(lambda x: x[1])

    # comvierto los strings en objetos JSON
    jsons2 = jsons.map(lambda j: json.loads(j))

    # incorporo el elemento timestamp en formato ISO en cada diccionario
    finals = jsons2.map(lambda d: changetimestamp(d))

    # escribo en el topic de salida de kafka (bikes)
    finals.foreachRDD(sender)

    ssc.start()
    ssc.awaitTermination()


if __name__ == "__main__":
    main()
