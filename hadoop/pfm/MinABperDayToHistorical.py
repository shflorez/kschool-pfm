# -*- coding: utf-8 -*-

"""Minumun Available Bikes Management.

Append minimun available bikes in the day for each station into a
historical file.
"""

from pyspark.sql import SparkSession
from datetime import date, timedelta
from hdfs import InsecureClient

namenode = "172.31.95.160:9000"
rawPath = "/pfm/raw/"
processedPath = "/pfm/processed/MinAB/"
din = "hdfs://" + namenode + rawPath
dout = "hdfs://" + namenode + processedPath


def getyesterday():
    """Retrieve yesterday."""
    datetoday = date.today()
    dateyesterday = datetoday - timedelta(1)
    return dateyesterday.strftime("%Y-%m-%d")


def appendorcreatehistoricalmin(row):
    """Append the minAB into the historical file if the station exists.

    Otherwise, create the historical file for that station.
    """
    hdfsclient = InsecureClient('http://172.31.95.160:50070', user='ubuntu')
    p = "/pfm/historical/" + row._c0 + "/"
    st = hdfsclient.status(p, strict=False)

    if (st is None):
        finalp = p + date.today().strftime("%d%m%Y%H%M%S%Z") + ".csv"
        hdfsclient.write(finalp, "ds,y" + "\n" + row._c2 +
                         "," + row._c1, encoding='utf-8')
    else:
        list = hdfsclient.list(p, status=False)
        for e in list:
            if e.endswith("csv"):
                finalp = p + e
                break
        hdfsclient.write(finalp, "\n" + row._c2 + "," +
                         row._c1, append='True', encoding='utf-8')


def main():
    """Run main function."""
    spark = SparkSession.builder.appName(
        "MinABperDayToHistorical").getOrCreate()

    # proceso de Append del fichero de medidas diario con el fichero histórico
    # de cada estación
    dfa1 = spark.read.csv(dout + getyesterday() + "-MinAB.csv", header="false")

    # por cada fila del fichero
    dfa1.foreach(appendorcreatehistoricalmin)


if __name__ == '__main__':
    main()
