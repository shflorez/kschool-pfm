#!/bin/bash

export HADOOP_HOME=/home/ubuntu/opt/hadoop
export YARN_CONF_DIR=$HADOOP_HOME/etc/hadoop
export SPARK_HOME=/home/ubuntu/opt/spark-2.1.1-bin-hadoop2.6
export PATH=$PATH:$HADOOP_HOME/bin:$HADOOP_HOME/sbin:$SPARK_HOME/bin

spark-submit --master yarn --executor-memory 1G --driver-memory 1G /home/ubuntu/pfm/FlumeToMinABperDay.py
