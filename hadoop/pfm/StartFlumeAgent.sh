#!/bin/bash

${FLUME_HOME}/bin/flume-ng agent --conf conf -conf-file ${FLUME_HOME}/conf/flume-kafka-source-hdfs-sink.conf --name agent -Dflume.root.logger=WARN
