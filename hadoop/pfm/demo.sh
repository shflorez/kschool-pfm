#!/bin/bash

cd ~/pfm

# Se arranca el agente Flume
nohup ./StartFlumeAgent.sh >> flume.out &

sleep 10

# Se lanza el job de Spark Streaming
nohup ./KafkaToDruidAdapter.sh >> kafka.out &

