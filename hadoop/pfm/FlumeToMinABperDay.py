# -*- coding: utf-8 -*-

"""Generate min available bikes for station in a day."""

from pyspark.sql import SparkSession
from datetime import date, timedelta
from pyspark.sql.functions import lit

namenode = "172.31.95.160:9000"
rawPath = "/pfm/raw/"
processedPath = "/pfm/processed/MinAB/"
din = "hdfs://" + namenode + rawPath
dout = "hdfs://" + namenode + processedPath


def getyesterday():
    """Retrieve yesterday."""
    datetoday = date.today()
    dateyesterday = datetoday - timedelta(1)
    return dateyesterday.strftime("%Y-%m-%d")


def main():
    """Run main function."""
    spark = SparkSession.builder.appName("FlumeToMinABperDay").getOrCreate()

    ydate = getyesterday()

    df = spark.read.json(din + ydate + "/*")

    df2 = df.groupBy("id").min('availableBikes').withColumnRenamed(
        "min(availableBikes)", "min")

    df3 = df2.withColumn("day", lit(ydate))

    df4 = df3.coalesce(1).write.csv(
        dout + ydate + "-MinAB.csv", header="false")


if __name__ == '__main__':
    main()
