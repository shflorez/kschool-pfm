#!/bin/bash

# Satori
echo "Copiando Satori"
scp -r ubuntu@ec2-52-87-161-185.compute-1.amazonaws.com:/home/ubuntu/pfm /Users/s.florez.losantos/Documents/__KSCHOOL/kschool-pfm/satori/

# Zookeper
echo "Copiando Zookeeper"
echo "Omitiendo..."

# Kafka
echo "Copiando Kafka"
scp -r ubuntu@ec2-52-23-253-137.compute-1.amazonaws.com:/home/ubuntu/pfm /Users/s.florez.losantos/Documents/__KSCHOOL/kschool-pfm/kafka/

# Hadoop
echo "Copiando Hadoop"
scp -r ubuntu@ec2-54-89-254-152.compute-1.amazonaws.com:/home/ubuntu/pfm /Users/s.florez.losantos/Documents/__KSCHOOL/kschool-pfm/hadoop/
scp ubuntu@ec2-54-89-254-152.compute-1.amazonaws.com:/home/ubuntu/opt/apache-flume-1.8.0-bin/conf/flume-kafka-source-hdfs-sink.conf /Users/s.florez.losantos/Documents/__KSCHOOL/kschool-pfm/hadoop/pfm

# Hadoop
echo "Copiando Druid"
scp -r ubuntu@54.227.133.226:/home/ubuntu/pfm /Users/s.florez.losantos/Documents/__KSCHOOL/kschool-pfm/druid/

