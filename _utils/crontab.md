## crontab -e

Configuración crontab en las diferentes instancias

#### Satori
```
* * * * * /home/ubuntu/pfm/satori-check.sh >> /home/ubuntu/pfm/check.out 2>&1
```

#### Master

```
30 0 * * * /home/ubuntu/pfm/FlumeToMinABperDay.sh >> /home/ubuntu/pfm/flume.log 2>&1
0 1 * * * /home/ubuntu/pfm/MinABperDayToHistorical.sh >> /home/ubuntu/pfm/historical.log 2>&1
```

#### Druid

```
05 00 * * 1 /home/ubuntu/pfm/MakePredictionsPROPHET.sh >> prophet.out
```