# -*- coding: utf-8 -*-

"""Satori client.

Stores the messages from Satori open data channel into a Kafka topic.
"""

import json
import threading
import time
from satori.rtm.client import make_client, SubscriptionMode
from kafka import KafkaProducer

endpoint = "wss://open-data.api.satori.com"
appkey = "e783a6168B0223BA277C15D15B2bFEb4"
channel = "NYC-Bike-Live-Station"
producer = KafkaProducer(bootstrap_servers=['172.31.90.185:9092'])
topic = "satori-nyc-bikes"


def main():
    """Run main function."""
    with make_client(endpoint=endpoint, appkey=appkey) as client:
        mailbox = []
        got_message_event = threading.Event()

        class SubscriptionObserver(object):

            def on_subscription_data(self, data):
                for message in data['messages']:
                    ##
                    mailbox.append(message)
                    got_message_event.set()

        subscription_observer = SubscriptionObserver()
        client.subscribe(
            channel,
            SubscriptionMode.SIMPLE,
            subscription_observer)

        while True:
            for message in mailbox:
                msg = json.dumps(message, ensure_ascii=False)
                producer.send(topic, msg.encode())
                # do not send the messages to fast for development
                time.sleep(0.1)


if __name__ == '__main__':
    main()
