#! /bin/bash

case "$(pgrep -f "python /home/ubuntu/pfm/Satori2Kafka.py" | wc -w)" in

0)  echo "Restarting Satori2Kafka.py: $(date)" 
    python /home/ubuntu/pfm/Satori2Kafka.py >> satori.out
    ;;
1)  #echo "Satori2Kafka.py running"
    ;;
esac
